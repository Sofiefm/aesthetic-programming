A program by [ Sofie F. Mønster](https://gitlab.com/Sofiefm/aesthetic-programming/-/tree/master) & [Nina Isis](https://gitlab.com/Ninaisis/aesthetic-programming-test/-/tree/master/minix9) <3

# Our API must have expired, normally GIFS would be displayed when the user presses a button. It has worked, but unfortunately does not work anymore

# [biased.Gif.Poetry();](https://sofiefm.gitlab.io/aesthetic-programming/miniX9/)

## About biased.Gif.Poetry();
Our program is based on an API from GIPHY and therefore our data is shown as different gifs. It is possible for users of the program to interact with it, by clicking on different buttons/words in the poem. By having this feature we hoped to increase the users curiosity and desire to reflect upon how algorithms that are implemented in software we use on a daily basis, is biased. This will be exemplified later in this readMe.

![Semantic description of image](biased.Gif.Poetry_example.png)


By using GIPHY we saw an opportunity to make the program entertaining for the user, while at the same time being able to express a deeper conceptual message. We did this, since we think it is really important to inform people about how software is inherently biased, and we hope to encourage everyone using this program, to have a critical approach when interacting with modern technologies. The data we get is structured by GIPHY’S own website and therefore it chooses the data we get. Since the data is real time it can actually change in our program anytime it changes on Giphy’s own website. In other words: Giphy’s website controls the content in our program, and they are able to limit our use of their API. We want to question the query and reflect upon how the gifs that appear are picked out, but also whom they are picked out by.


## Can you describe and reflect on your process of making this mini exercise in terms of acquiring, processing, using and representing data?
We began our design process by having long conversations about different API’s and how we should use it in our work. We spent a lot of time deciding which API we should use, but it turned out that a lot of them were difficult to access, and therefore we ended up using GIPHY which is available for everyone. We also discussed how the API’s reflect the infrastructure of the internet and is a clear signifier for how data flows. We spent a lot of time reading and trying to understand how the use of API’s influence different websites, and even though we had a critical approach we also saw how API's can have a lot of benefits!

We agreed that we wanted to make a simple program since it is the first time (for both of us) that we work with API. However we kept bringing up new topics that would be interesting to express through our work, and since both of us were really inspired after last week's topic which was “vocable code” we agreed to combine this with the use of API’s. This led us to this e-lit work which takes shape as an interactive poem, a poem written by Mark Tay, where the user gets to experience the gifs without knowing the bias embedded within. While we couldn’t choose the exact GIFS that appear, we had the power to decide which keywords were used in the search by changing the query of each ‘button’. Ex:


function blood() {
if (bloodb.mousePressed) {
getQueryData("&q=breonnataylor+icantbreath+femicide+menstruation+meatindustry+policeviolence");
}  }


Here you can see we have taken the word blood and embedded meaning into it in the form of a specific query. This has an impact on which gifs will be posted when the button is pressed. Though we also noticed how some of the keywords would be a lot more prolific than others. This could probably be due to some keywords having a lot more tagged gifs, but there are also some words which could seem like they were censored.
Because the bias in our program isn't apparent without looking at the sketch.js file we thought it would be funny to call the program "biased.Gif.Poetry();".


## How much do you understand this data or what do you want to know more about?


GIPHY is the second largest search engine in the world with more than 10 billion search pr. Day — so you must really hope that the algorithms behind this huge company has their ethics under control. This is exactly what we wanted to explore in our work, so we did a lot of background research to see if there were any specific topics we should focus on, but also to figure out who owns and controls GIPHY. It does seem like Giphy is pretty open and has there algorithms in check for the most part we did notice they are owned by the little company with the name of Facebook(don't know if you have heard of it? :))..this makes GIPHY and it's API a lot less neutral in our opinion.


## How do platform providers sort the data and give you the selected data?


We noticed when trying to acquire access to different API's we often had to go through different sign up processes, and were also made aware of their limited use, and that companies are very interested in knowing what and how you will be using their API/data.


## What are the power-relations in the chosen APIs?


If we think about the power-relations between GIPHY (data distributor) and us (the users) the first thing that comes to mind is that we need an API-key to be able to access the JSON file where their data is stored. This means that Giphy has the ability to track how we use their data in our own program. Since Giphy has the reserved rights to deny our access to their API at any time, it could be argued that they have more power than us. However we don’t pay (with money) for accessing the API, but it might be worth reflecting upon the data / personal information we have to give up to access it. We could also think about it as a two way data exchange between us and them, however it is not clear for us what we actually give in exchange (unless you really dig in and research or inquire about it, which a lot of people probably don’t usually do). All of this leads to different questions such as;


What are we really exchanging?


Is it a fair deal?


Maybe the users should claim more specific information about what they are exchanging since we 100% know what we get, but not what we give….
Is it even our right to know?


What are the consequences of this data exchange?


We conclude that the power relation between the API user and the distributor aren’t equal, since we need more information and honest communication about this. It is difficult for the user to know exactly what we are exchanging, we leave the knowledge to the distributor, which we argue leads to an unequal relation in power.


## What is the significance of APIs in digital culture?



As Winnie Soon and Eric Snodgrass mention in their paper "API practices and paradigms" APIs act as links and facilitators for an exchange of data and information.
> “At a basic level, APIs do the work of exposing and making available the resources of particular programmatic components so that they can be accessed by other programmatic components. APIs typically do so with a view to facilitating forms of exchange and interoperability between these components and the corresponding applications and/or larger systems within which they are situated.” (Snodgrass and Soon, p. 5) 

In this way you could say that API in it self is a somewhat neutral tool that only in the context of user/data/data distributor derives some sort of deeper meaning. It is this intersection that make APIs a very central part of digital culture. because many different people that are involved with technology intersect, both users, developers, and the companies. we would argue that it's significance is huge, though again there are certain power structures and also a lack of transparency that we have to be aware of, and question, as API’s quickly can become a front for something that seems more transparent/open source than it really is.


## Try to formulate a question in relation to web APIs or querying/parsing processes that you want to investigate further if you have more time:


how is bias in data further distributed without question, and how does this further uphold power structures / perpetuate these?


### xoxoxox Nina Isis & Sofie F. M. <3 <3 

## Reference list:

This video tutorial inspired us to use a GIPHY API and how to start the coding process:
https://www.youtube.com/watch?v=mj8_w11MvH8&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r&index=10

This is the full length of the poem we included in our runMe:
https://medium.com/be-unique/technology-is-changing-the-world-77a21785c38a

API source:
https://giphy.com/

Gif  https://gifer.com/en/6stc

http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf
