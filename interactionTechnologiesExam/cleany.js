
var yesButton = document.getElementById('yes');
var noButton = document.getElementById('no');


function toggleYes () {
  yesButton.classList.add('yesColor');
  noButton.classList.remove('noColor');
}

 yesButton.addEventListener('click', toggleYes);


 function toggleNo () {
   noButton.classList.toggle('noColor');
   yesButton.classList.remove('yesColor');

 }

  noButton.addEventListener('click', toggleNo);
