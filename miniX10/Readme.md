# Algorithmic Procedures 

## Individual work 

I chose to make a flowchart based on [miniX7](https://gitlab.com/Sofiefm/aesthetic-programming/-/tree/master/miniX7) (objected oriented programming), I think this has been the most technically complex task so far, and I wanted to understand the code behind the program better. I had a difficult time deciding how to construct my flowchart as well as figuring out what part of the process I highlight in it. In the end, I decided to make two different flowcharts based on mini exercise seven. 

The purpose for the first flowchart that I created, was to visualize the processes behind the final program, it is organized so that it’s easy to get an overview of what processes the different js files in the program should contain. Even though I made this program myself, I find it confusing to organize the sequence of all the different steps, since it was hard to remember, what exactly I was doing a couple of weeks ago. I went thoroughly trough the code from start to finish, and then I implemented the most important steps in the flowchart, ass you see below. I chose to mark each js file with a different color, to make it less confusing to look at. After making this flowchart I realized how I can benefit from doing this in the future, I think it will really help me to structure my work, but also to think in a new and more innovatively way! 


![Semantic description of image](flowchart1.png)

For the second flowchart I made, I wanted to focus more on the final outcome (the runme) and visualization of what will happen when the user interacts with the game. The game ended up being really, really simple, which is also reflected in the second flowchart (look below).

Somehow, I would have wished that I made a flowchart, when I was making/designing mini exercise seven. If I had done that, I think that I would have explored multiple different ways to create this game and hopefully it would have had some more features. Making a flowchart would have benefitted my design process, since it is easier to plan the coding process, if you know exactly what you want to do when you start coding. I remember, how I thought about a lot of different elements that I could have added to the game, but I had a hard time structuring the code already, so I had to give up those ideas and stick more to the Daniel Schiffman’s video, that I was inspired by. 


![Semantic description of image](flowchart2.png)

## Group work 

### Idea number 1
Our first idea, is to create a "game", where it will be possible for the user to interact with it. On the canvas there will appear a picture and two collections of tags. One of the collections is generated and thereby biased by us (the human creators of the program) and one is generated from an API called EveryPixel (if we can make it work). The user then has to choose which tags carry the most resemblance with the photo on the site. The program is meant to put focus on both the bias we as humans have for certain pictures and how the machine will perceive the same picture.

![Semantic description of image](flowchart3.png)

### Idea number 2 

Our second idea is based on the topic "vocable code", and therefore this idea is based on generative poetry. This idea is based on the same API from the abobementioned idea (we are actually also thinking about how we could combine this two ideas). Everytime a new photo is shown the tags that the API comes up with will generate a poem on the right side, which means that everytime a new picture appears a new poem will pop up. Some words and lines of the poem will be static and made by us and then the tags will be used to fill out the rest of the poem. The pictures will change and so will the poem. The outcome will be a generative poem that relates to the displayed picture.

![Semantic description of image](flowchart4.png)


## What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?


For the flowchart it has been a bit difficult to be 100 percent clear in the representation of the process, before we've started actually programming the things we ideally want to be present on the site. We found that it was actually a good exercise for us, because it forced us to be very specific in each category of the programming. This also led us to be even more concordant in regards to starting up our final project.
Since none of us are originally programmers our thoughts tend to focus more on how we want to present the program aesthetically and conceptually, but this way of starting the process also made us think of how to do it in coding, which has been super helpful. This also relates to the level of abstraction that is important when making a flowchart, to whom the flowchart is adressed to.


## What are the technical challenges facing the two ideas and how are you going to address these?


For both of our ideas we would like to use the EveryPixel API to tag some pictures that we choose. Our very first technical challenge has been getting access to this API, which is still something we’re working on. If we can’t get this to work we might have to consider using another similar one, or rethinking our entire idea.


For the first idea we’ll have some restrictions in generating our own tags. Since we have limited time we’ll never be able to generate enough tags for endless amounts of pictures. This means we’ll have to have a limited amount of pictures.
For the second idea we are facing some technical challenges in terms of what source we should use - API or JSON. We were considering the API since it may seem easier to gather the pictures with the right tags that belong to the picture. In that way we would be sure that the tags are matching the giving picture that is shown on the canvas. Furthermore, we faced a few difficulties with the API’s. We think that it might be too complicated to create JSON files where the tags and the pictures are seperated. This would also take away the aspect of the machine learning - since it would not be generated from the actual picture, but randomly instead.


Earlier on in the process we were looking into various API’s, for example from Twitter, but this would mean that we had to wait for a long period of time before we could gain access. Twitter also wanted to know the specific purpose of our project, what our backgrounds were, and what methods we were using to achieve our results. Based on the complications we chose not to work with the Twitter API.


## In which ways are the individual and the group flowcharts you produced useful?


As mentioned above the group flowcharts have been very useful for creating a collective idea and understanding in everyone's mind about the conceptual and technical ideas for the runme. The flowcharts also makes it easier to begin the design process of the program because the basic functions have already been made. Another thing is how later on it will be easier, with this flowchart, to change and develop the program together in a collaborative mind by breaking down the program in to smaller pieces. This also means that it is quite suitable for group work because you can split the different parts of the program and work on them individually.
The individual flowchart is useful for rethinking our original thought process. By working backwards in this manner, we were able to reflect on why we made certain decisions in our program. This has a lot of value for us going forward in the programming process, both individually, but also for us as a group.



