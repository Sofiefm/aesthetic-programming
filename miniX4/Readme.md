This links to my miniX4: https://sofiefm.gitlab.io/aesthetic-programming/miniX4/ 

#### The Tricker

#### Short description: 
This program (The Tricker) is inspired from the headline “Capture All” and is all about showing how easy a person can get tricked into sharing personal information about themselves when using digital devices, and also to visualize how everything you do on the internet is being captured and saved. The idea for this visualization is that all the data you share will appear on your own screen, in this way you get to see the output / the data that are being captured. When the user presses the “done” button, all the data will disappear, but it will only disappear from the user herself, the data will forever be kept, so it can be used for and against her. 
Everything you do online leaves a trace of something do be captured and this program is made to visualize exactly that. Even though it is made as some sort of entertainment, it is also a way to make people aware of how their actions will be captured and used for statistics, advertisements and so much more. 

![Semantic description of image](the_tricker.png)

#### Description of my program: 
As told in the previously section I made this program to kind of visualize how your data is always being captured. I decided to use childish colors to make my program look friendly and welcoming, so the user might not think of the consequences of them writing personal things about themselves. Also, I wanted to create a program where it would be possible for the user to personalize the canvas, by writing whatever they want. To make this code I’ve used some different DOM elements such as createButton and createElement. Furthermore I used mousePressed to make the button’s work by creating something if they were pressed and I used button.position to place the different elements on the canvas. 

For this miniX I’ve learned a lot by looking at other examples and specific parts of their code. I tried to make the mic work, but I had some troubles figuring out how to change the link from HTTP to HTTPS via atom-live-server, so I decided to delete that part of my program, since this week has been very busy, and I didn’t really have the time to figure out this problem. I tried to run my code in three different browsers, but nothing seemed to work. All of a sudden it worked in Safari, but only one time, and as soon as I reloaded the window, I couldn’t make it work again??? The sound wasn’t part of my conceptual idea anyway, so maybe I will try it for the next miniX. 

#### How my program and thinking address the theme “capture all”
When I read the required reading for this week, my first thought was to create an example of how people’s data are being captured. It feels like every click, every search and every video are being watched and stored, and that this is used both for and against us. I have never really been sure about when I was being watched and even though I probably knew it was happening I still accepted every cookie, and sometimes clicked “ok” to a pop-up window before even reading what I was allowing it to do. My program addresses the theme capture all by highlighting how data is being captured and even though it is a very important subject, there is still some irony, which is seen in the “end” of my program when pressing the “done” button. The final program symbolizes how the more personal information or data in general you share, the more they know about you, and that’s why the words the user writes are shown in the canvas. My “done” button makes it noticeable that the user has a task that needs to be done, before they are supposed to press it, which might be motivating for some. This is also what the core text book by Winnie soon and Geoff Cox discusses:
“A button is “seductive,” with its immediate feedback and instantaneous gratification. It compels you to press it. “ (Soon og Cox). 
When we see a button, it seems like the most natural thing to interact with it, which I also hope people will feel the need to do when using my program. When the submit button is pressed they will realize how they can personalize the canvas, and this is the entertaining part of the program, when they finally press the done button and the text shows up, the entire message of the program become clear, and I hope it will leave stuff for consideration. 

![Semantic description of image](the_tricker_1.png)


## References 

Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

