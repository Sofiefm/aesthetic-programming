var input; //The input box
var button1; //Submit button
var title; //Title for the input box
var mic;
var wordCount = 0; // How many words the user has written

function endScreen() {
  var thankyou; //The last text in the program
  clear();
  background(155, 232, 247);
  thankyou = createElement('h1', 'Thanks for the info! I will keep that forever!!');
  thankyou.position(0, 0.8 * height);
  thankyou.center('horizontal');
  //textAlign(CENTER);
}

function setup() {
  // I decided not to remove the audio from the program, to see if it's working after I upload it to Gitlab
  mic = new p5.AudioIn();
  mic.start();
  createCanvas(windowWidth, windowHeight);
  background(155, 232, 247);

  //This part of the code is where to user can write their favorite food
  input = createInput();
  input.position(width / 2 - 110, 220);

  //This is the code for the submit button (button1)
  button = createButton('Submit');
  button.position(input.x + input.width, 222);
  button.mousePressed(answer);

  //The "done" button
  button = createButton("DONE");
  button.position(width / 2 - 100, 535);
  button.mousePressed(endScreen);


  //This is for the title of the input box
  title = createElement('h5', 'Write 5 things that made you happy today :)');
  title.position(width / 2 - 125, 140);

  textAlign(CENTER); //Position of the text
  textSize(20); //Size of text that appears after the submit button is pressed.
}

function draw() {
  var vol = mic.getLevel();
  console.log(vol);
  ellipse(50, 100, vol * 300, vol * 200);

  //the blue square — what you interact with
  stroke(255, 205, 241);
  strokeWeight(10);
  noFill();
  rectMode(CENTER);
  rect(width / 2 - 1, height / 2 - 1, 270, 420, 10);
  noStroke();
  fill(247, 173, 227);
  rect(width / 2, height / 2, 250, 400, 5);

  //Code for counting how many things the user has written

  if (wordCount > 0) {
    push();
    fill(77, 219, 247);
    textSize(150);
    text(wordCount, 0.5 * width, 0.5 * height);
    pop();
  }

}

  //This is the function where the answer from the user will be shown, it will work if mousePressed on the submit button.
function answer() {
  const name = input.value();
  wordCount++;


  //I make a for loop to place the answer all over the canvas
  for (let i = 0; i < 200; i++) {
    push();
    fill(random(255), 200, 255, 100);
    translate(random(width), random(height));
    rotate(random(2 * PI));
    text(name, 0, 0);
    pop();
  }

  function windowResized() { // I make this function to make sure, that the program will fit the entire screen.
    resizeCanvas(windowWidth, windowHeight);
  }
}
