# MiniX3 – Design a Throbber

This links to my Runme: https://sofiefm.gitlab.io/aesthetic-programming/miniX3/

#### What I want to express 


In this week’s miniX3 we had to think about temporality in digital culture focusing on the throbber icon. I looked it up on Pinterest to get some inspiration, I found inspiration for the concept of my own throbber, but I created the look of it myself. Conceptually I wanted to create something meaningful to illuminate an important political topic. Now that we live in 2021, I have a hard time understanding how some people can’t accept people who are different from themselves. When it comes to such a private matter such as love or sexuality, I really believe that people should do whatever they want to make them happy. This is the main reason why I chose the design of my throbber; I know that it is probably not going to change anything, but I believe that it’s a good start to just talk about it and the visualization will maybe get people to think more about this topic and hopefully this will lead to a more embracing society in the future. I added the text to my program to spread a positive vibe “waiting for love to win”, by writing this I think everyone who has ever been in love themselves will agree and maybe change their mind about the QUEER society to the better. I chose a neutral background for my design, since the throbber itself is very colorful. The neutral background can also be seen as a metaphor, saying that behind our charisma and sexuality we are all JUST humans, so let’s support each other!! 

![Semantic description of image](rainbow_throbber.png)


#### The time-related syntaxes and functions that I have used 


In P5 there is a loop which is always there. To control this loop, I used the framerate to decide how many times pr. second my draw function should be called. This is also how I decide how fast my throbber should rotate. I use push and pop to draw the front row of the rainbow at a given angle, then I use modulo (%) to make my throbber a cycle. Usually, the frame count is linear but by using modulo and deciding the angles I make it a cycle. Modulo determines how many frames should run to make it start over. In this case it is 30 and this is how my throbber is visualized as a loop. This means that I have changed it from a forever growing number to something that is cyclical. In Winnie Soon’s text “Throbber: Executing Micro-Temporal Streams” she talks about how a throbber can be seen as a good visual icon of something happening (loading) or time passing by. This is exactly what I want my throbber to express. Time keeps on passing by but behind the surface progress is happening as communities all over the world are evolving into becoming much more tolerant and accepting towards the QUEER society. In other words: progress happens while time passes.  


#### A throbber that I have encountered in digital culture 


When I think about throbbers that I encounter in Digital culture I don’t associate it with something positive. I would actually define it as something I find really annoying — but maybe that has something to do with me not being patient enough. A throbber communicates lack of network or in some cases the problem is that the file is big and therefore takes a while to load. When I visit my grandparents vacation house which is in a remote location in the countryside, there is always really bad internet connection. Therefore, I often encounter throbbers when trying to use social medias such as Instagram or Facebook, which I see as a disruption in my flow, and this is probably why I find it so annoying. I tried to find a solution for this in my design, and therefore I created something political, which encourages to reflection and discussion for the viewer. Furthermore, I think that I created something interesting and exciting to look at. 


### References

Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
