var textCount = 0; // Here I assign my global variable
var textColour; // Here I declare my global variable

function setup(){
  createCanvas(windowWidth,windowHeight);
  frameRate(15); // I set my frameRate to control the speed of how fast the rainbow is moving
  textColour = color(random(255), random(255),random(255)); // Here I assign my variable
}
function draw(){
  textCount = textCount + 1; // I do this + line 12 to make the colour on the text change
  if (textCount > 10) {
    textColour = color(random(255), random(255),random(255)); // Here I decide the colour on my text should be random
    textCount = 0;  // This line resets the textCount
  }
background(255,60);// I use the alpha to adjust the trancparency of the background

// I make the text for my program
strokeWeight(3);
fill(textColour);
textSize(50);
text('Waiting for love to win', 720, 600);
textAlign(CENTER);


//Here I customize my own function so it is easier to seperate the code
drawElements();// I chose to call this function drawElements, since this is where I will put all my ellipses

function drawElements(){ // I isolate this function
  let num = 30; // Here I assign a local variable

push(); // I use push and pop to isolate the rotation part of my code
  translate(width/2, height/2); // I do this to make my ellipses stay in the middle of the screen
  let cir = -180/num*(frameCount%num); // Here I calculate the angle of the rainbow in my program. Since I only want semi circle I have a negative number
  rotate(radians(cir)); // I set the rotation of the ellipses

// I make all of the ellipses in the rainbow
  noStroke();
  fill(255,0,0); //red
  ellipse(175,1,60,70);
  fill(255,162,0); // orange
  ellipse(150,1,55,65);
  fill(255,255,0); //yellow
  ellipse(125,1,50,60);
  fill(0,255,0); //green
  ellipse(100,1,45,55);
  fill(0,0,255); //blue
  ellipse(75,1,40,50);
  fill(171,0,255); //purple
  ellipse(50,1,35,45);

pop();

}
function windowResized() { // I make this function to make sure, that the program will fit the entire screen.
  resizeCanvas(windowWidth,windowHeight);
}
}
