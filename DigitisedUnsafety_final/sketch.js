var answers;
var noteBook;
var lastText;
var delayTime;
var currentSeconds;
var timeDifference;
var tellTheStory ;

function preload() { //preload
  femaleExperiences = loadJSON("answers.json");
  noteBook = loadImage('baggrund.png');
}

function setup() { //setup
  createCanvas(windowWidth, windowHeight);
  frameRate(1);
  delayTime = 10;
  lastText = second();
  tellTheStory = random(femaleExperiences.differentAnswers);
}

function draw() { //draw
  currentSeconds = second();
  background(0, 0, 0);

  //Placering af vores baggrundsbillede
  image(noteBook, 0, 0, 1280, 832);
  fill(255, 255, 255);
  textStyle(BOLD);

  textFont('helvetica');
  textSize(20);
  text(tellTheStory, 150, 275, 400, 600);

timeDifference = currentSeconds - lastText

if (timeDifference<0) {
  timeDifference+=60
}
 if (timeDifference>=delayTime) {
 lastText = currentSeconds
 tellTheStory = random(femaleExperiences.differentAnswers);
 }
 console.log(currentSeconds);
}
