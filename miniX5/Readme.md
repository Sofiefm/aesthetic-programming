# Mini X5 // A generative program 

Link to my runme: https://sofiefm.gitlab.io/aesthetic-programming/miniX5/ 

Link to my code: https://gitlab.com/Sofiefm/aesthetic-programming/-/blob/master/miniX5/sketch.js 

## Description of the rules in my generative program:

In the beginning of the process, I made up two very simple rules for my program. I wanted to keep it simple and minimalistic. 


1.	Every time the program resets, the background should change color. 
2.	Every time the page is being refreshed the circles in my program should create a new pattern. 


I used random for both of these rules, but I did, however make a constraint for both the size and the color of the circles. So even though I used random elements to create this program, I still controlled how random the output could become. After these two rules was implemented, I still felt like something was missing, so I made up two new rules. 


1.	Every time the circles move randomly, they will change size. 
2.	Every time the circles move “outside” of the canvas, a new one will appear on the opposite side of the canvas. 


I made these rules to make the output more aesthetic and beautiful to look at. The last rule: “Every time the circles move “outside” of the canvas, a new one will appear on the opposite side of the canvas” makes the program seem like it is moving continuously, and I think that this rule really completes the program.

## Description of how my program performs over time and produces emergent behavior: 

The program is based on different “branches”, which I chose to call them. In the beginning it is set to create three different branches, which will grow in different directions on the canvas (see the picture below). The branches always move randomly across the canvas and over time the canvas will be filled with circles in different colors and sizes, this might take some time, so you will need patient to experience this. Besides from the size of the circles I used the framerate to control how much time it will take before the entire canvas is covered by the circles. 

![Semantic description of image](three_branches-png.png)

When creating a generative program and at the same time making specific rules for this program, it is really interesting to think about how the “designer” has an impact on the outcome of the final program. My program could for example be much more random, but I chose to make it specific to some extend since I wanted to create something, that in my personal opinion is nice to look at. This leads to the question about whether it is the computer or the designer behind the program, who is the artist? In my opinion I see it as a cooperation between the designer and the computer, since they are equally dependent on one another, the designer tells the computer what to do, and the computer executes it. 

The role of rules and processes in my work: 
The rules have a huge impact in my work, since they are the reason that the circles are getting instructions to move and expand the way they do. The instructions make them move automatically, without any further guidance is necessary, which makes it seem like they behave freely. Every time the program resets the user will see a new pattern and color of the background. 

![Semantic description of image](canvas_is_covered.png)

## How to understand auto-generator considered to the assigned readings and this mini exercise: 


My program can be described as generative art, since it focuses on the outcome which is evolving without the programmer (me) being in complete control of the end result. This program is a result of an unpredictable process controlled by the computer. Since the program is auto generated, the program will keep on running, and it is up to the user herself, to decide when they experience the program as completed. The user also has the power to reload the browser, if they want to change the background color or the circles that the code creates. They can change the output of the program until they are satisfied, by changing the elements they are still able to affect. 
All of this made me think if anything can be truly random when it comes to computing? By given a set of rules and other instructions we design the program to do specific things and thereby we might ruin the ability for the program to be autonomous? 

In the chapter “Randomness” in “10 PRINT CHR$(205.5+RND(1));, it is discussed how randomness in computers are actually pseudorandom. The random function appears to be truly random, but that’s not correct, since the random function relies on you setting up rules and by that telling the computer what to do from a predefined selection, this means that the randomness of a program is probably always controlled to some extent.. We might never know what is going to be chosen next, but we will know from what category it will be chosen from. Montford presents how everything in computing is actually pseudorandom, but if we put it into a wider perspective, it can be discussed if everything is in reality pseudorandom, since there is always a reason or explanation of why something is happening? 


## References
•	Montfort, N, et al. "Randomness". 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, The MIT Press, 2012, pp. 119-146
