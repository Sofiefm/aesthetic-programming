//My local variables

var branches = [];
var start_branches = 3; //number of branches in the beginning of the program

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(17); // I use the framerate to control how fast I want the circles to expand on the canvas.

  for (let i = 0; i < start_branches; i++) {
    branches[i] = new Branch();
  }
  /* Rule
  The background is a random value between 180 and 255
  for each rgb value everytime the page is refreshed,
  I chose this since I prefer the brighter colors*/
  background(random(180, 255), random(180, 255), random(180, 255));
}

function draw() {
  for (let i = 0; i < branches.length; i++) {
    branches[i].show();
    branches[i].move();
  }

}

class Branch {
  constructor() {
    this.x = width / 2; //the starting point
    this.y = height / 2; //the starting point
    this.size = 12; //Maximum size of the circles
  }

/*Rule
how all the circles should move on the canvas and how far apart from eachother*/
  move() {
    this.x = this.x + random(-10, 10);
    this.y = this.y + random(-10, 10);


    /* Rule
    In this part of the code,
    I make it possible for the ellipses to "teleport" to the opposite side of the canvas,
    when it reaches any sides of the canvas.
    This is made to create a sense of continuity. */

    if (this.x < 0) {
      this.x = width;
    }

    //if the x value is greater than width it will continue its path at 0
    if (this.x > width) {
      this.x = 0;
   }
    // I do the same with the y-values to make the circles expand on the y-axis
    if (this.y < 0) {
      this.y = height;
    }

    if (this.y > height) {
      this.y = 0;
    }

  }

  show() { //What will actually appear on the canvas
    fill(random(180,255), random(180,255), random(180,255)); //I use the bright colors again, so i matches the background.
    noStroke();
    //Rule
    circle(this.x, this.y, random(this.size), random(this.size));
  }

}

function windowResized() { // I make this function to make sure, that the program will fit the entire screen.
  resizeCanvas(windowWidth, windowHeight);
}
