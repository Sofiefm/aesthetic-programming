//Here I declare my variables
var large;
var small;
var backgroundColor;
var scaleFactor;
var boxWidth;
var strokeWidth;
var d;
var circleX;
var circleY;
var verticalWidth;
var verticalHeight;
var horizontalWidth;
var horizontalHeight;
var height;
var symbolColor;
var verticalX;
var verticalY;
var horizontalX;
var neckHeight;
var horizontalY;

function setup(){
  //Here I define my variables
  scaleFactor = 0.5;
  boxWidth = 255;
  strokeWidth = 40;
  d = boxWidth - strokeWidth;
  circleX = 0.5 * boxWidth;
  circleY = 0.5 * (d + strokeWidth);
  verticalWidth = 10;
  verticalHeight = 190;
  horizontalWidth = boxWidth;
  horizontalHeight = verticalWidth;
  //Here I calculate height of the createGraphics (line 37 only)

  boxHeight = d + 0.5 * strokeWidth + verticalHeight;

  verticalX = 0.5 * boxWidth - 0.5 * verticalWidth;
  verticalY = boxHeight - verticalHeight;
  horizontalX = 0;
  neckHeight = 60;
  horizontalY = d + strokeWidth + neckHeight;
  // I use the frameRate() to decide how often I want the background to change.
  frameRate(0.4);

  createCanvas(windowWidth, windowHeight);

  symbolColor = color(238,79,232);
  backgroundColor = color(random(255), random(255), random(255));

  large = createGraphics(boxWidth,boxHeight);

//Now I use createGraphics to create a small canvas for the smaller symbol

  small = createGraphics(boxWidth * scaleFactor,boxHeight * scaleFactor);
}

function draw(){
  //This is my drawing code


  // Here I set the background so I am sure it will happen again and again.
  backgroundColor = color(random(255), random(255), random(255));
  background(backgroundColor);

  // This is how I created the large symbol
  large.background(backgroundColor);

  //The circle in top of the emoji
  large.noFill();
  large.stroke(symbolColor);
  large.strokeWeight(strokeWidth);
  large.circle(circleX, circleY, d);

  //The vertical rectangle of the emoji
  large.fill(symbolColor);
  large.rect(verticalX, verticalY, verticalWidth, verticalHeight);

  //The horizontal rectangle
  large.rect(horizontalX, horizontalY, horizontalWidth,horizontalHeight);

  //This is how I created the small symbol

  //The circle in top of the emoji
  small.background(backgroundColor);
  small.noFill();
  small.stroke(symbolColor);
  small.strokeWeight(scaleFactor * strokeWidth);
  small.circle(scaleFactor * circleX, scaleFactor * circleY, scaleFactor * d);

  //The vertical rectangle of the emoji
  small.fill(symbolColor);
  small.rect(scaleFactor * verticalX, scaleFactor * verticalY, scaleFactor * verticalWidth, scaleFactor * verticalHeight);

  //The horizontal rectangle
  small.rect(scaleFactor * horizontalX, scaleFactor * horizontalY, scaleFactor *  horizontalWidth, scaleFactor * horizontalHeight);

  //Here i place all the big signs

  image(large,560,400);
  image(large,10,350);
  image(large,850,0);
  image(large,300,0);
  image(large,1150,350);

  //Here i place all the small signs

  image(small,10,0);
  image(small,330,600);
  image(small,620,10);
  image(small,920,600);
  image(small,1200,0);
}
