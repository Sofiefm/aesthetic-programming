# This is the description of my miniX2

This links to my first emoji: https://sofiefm.gitlab.io/aesthetic-programming/miniX2/emoji_1/


This links to my second emoji: https://sofiefm.gitlab.io/aesthetic-programming/miniX2/emoji_2/

# Description of my program
For both of my emojis I wanted to represent something missing in the regular emoji repository, therefore I spend some time just thinking about what kind of emojis I wish was available for me to use on my Iphone. I will get into more details about what my emojis are representing and why I think it is important later. 

#### Female Symbol Emoji
My first emoji is representing all women in the world and the idea behind it is that all women should support each other! I chose to make this emoji, since I personally would use it a lot, and I think it would be a cool emoji to use if you want to show love or support to other women. Since this emoji is about a global female community, I chose trough my code to show multiple symbols on the same screen. The message is that we are never alone and with love and support you can do much more than you think you can do. I made the background change to a random colour with the frameRate(0,4) besides from the aesthetic look the message is, that it doesn't matter where you are from or what kind of social, political or geographic background a person has we should never be judgemenfull towards eachother. The main message is to be supportive and loving to all females all over the world. 


I spend a lot of time making this emoji.. First I declared all my global variables and in the function setup() I then defined them. I used quite a lot  math and logical thinking to figure out this part. During the process I fugured out that I had to rename several names of my variables since they were already a part of the p5js references (for example width and height). For the function draw() I used all of my variables to create the symbols I had to put both the large and the small symbol in this part of the program to make it appear on the screen. 

![Semantic description of image](female_symbol.png). 


#### Climate Crisis Emoji
My second emoji is repesenting the climate crisis — I choose this topic since I found a lack of this in my online research. My first thought was to recreate the regular earth emoji and change the colours of it to kind of simmulate that the earth is "burning" or is on fire. In the beginning I thought I was able to create the earth in the code by myself, but I quickly realized that this was not possible (at least not for me), instead I uploaded a picture of the regular earth emoji and after I just had to change the colour of the picture, to get the "the earth is burning" effect, I used the function tint()to do that. I imagined that handeling the picture and changing the colour of it would be the most difficult part, but that was actually quite easy! I used the functions loadImage() and image() to get my picture shown in my program. After this I got to the part where I had to practice making geometric shapes, I found it easy to do since I used the ellipse for my emojis mouth, and that was one of the first things we learned how to do. For the eyes of my emoji I also used ellipses and circles, so that wasn't a problem either. The most difficult part for me was to make the eyebrows of the emoji I used curveVertex() for the first time which I found a bit confusing and it took me a while to figure out that the first and the last coordinate had to be repeated.

Now that the face of the emoji was set I wanted to make it interactice somehow and my first thought was that the viewer should be able to "save the earth" so I used the function if(mouseIsPressed). The idea was to click on the mouse and then the earth should turn green/blue and happy again. 
![Semantic description of image](burning_earth.png). 

Here you can see a screenshot of the result! I decided to keep the eyebrows and eyes of the emoji and therefore I just had to change to mouth and the colours, but that turned out to be enough to make a huge difference in the perception of the emoji. To make the "happy mouth" I used curveVertex(); again and this time I found it a lot easier since I had already done it on the sad part of the emoji. Now that my program was interactive and both "sides" of my emoji was finished I though it would be fun to add some text which gave the emoji even more expression. 

![Semantic description of image](happy_eaarth.png). 

#### My emojis in a social and cultural context 
I think that both of the emojis I created has a very important message. In my opnion it is important to create emojis like theese since emojis is no longer just yellow circles with a facial expression. Today emojis can also be seen as a tool to express your political, social or cultural opnions within different topics, which means that an emoji can be a reflection of how you see the world. During the last couple of years emojis has become more and more realistic. In Modifying the Universal, Abbing, Pierrot and Snelting focuses on how big tech-companies such as Apple have had challenges considered to to the identity politics which has led to an arrising awareness of this. They also mention that we are witnessing a sexist backlash in terms of discrimination of women on one side and the development of affirmation strategies on the other side.

Since the climate crisis is a global concern I wonder how the only emoji representing it is the recycle symbol. I think my emoji representing the climate crisis is a very good symbol for how the earth is actually on fire severel places around the world. I especially think about the Australian fire in the beginning of 2020 which was covered by the media all over the world, you would think an incident like that would maybe get the big tech-companies to be more aware on how the climate crisis is represented through emojis. Emojis is also a clever way to get people to draw attention to certain topics.


## References

Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51

Earth globe png: https://emojiisland.com/products/earth-globe-americas-emoji-icon 




