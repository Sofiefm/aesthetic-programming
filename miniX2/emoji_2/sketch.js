let img;

function setup(){
  //This is my setup code
  createCanvas(windowWidth, windowHeight);
img = loadImage('earthglobe.jpg');
}

function draw() {
  var mouthColour;
  var lipColour;

  //This is my drawing code
  if(mouseIsPressed){
    background(0,230,0);
    image(img, 400, 0);
    noTint();
    mouthColour = color(96,213,209);
    lipColour = color(87,230,130);

    //The shape of the happy mouth
    strokeWeight(2);
    stroke(2);
    point(617, 366);
    point(796, 366);
    point(769, 460);
    point(710, 482);
    point(634,448);
    point(609,394);
    point(615,368);
    strokeWeight(5);

    strokeWeight(4);
    stroke(33,85,186);
    fill(126,250,255);
    beginShape();
    curveVertex(617, 366);
    curveVertex(617,366);
    curveVertex(796, 366);
    curveVertex(769, 460);
    curveVertex(710, 482);
    curveVertex(634,448);
    curveVertex(609,394);
    curveVertex(615,368);
    curveVertex(615,368);

    endShape();

    //Text if the mouse is pressed
    strokeWeight(2);
    fill(0,255,0);
    textSize(50);
    textAlign(RIGHT);
    text('Thanks!!', 800, 760);
    textAlign(CENTER);

  } else {
    background(238,76,26);
    image(img, 400, 0);
      // I decide the colour of my image
    tint(220,0,0);
    mouthColour = color(150,40,40);
    lipColour = color(233,36,36);
    //Mouth of the sad emoji
    fill(mouthColour);
    stroke(lipColour);
    strokeWeight(5);
    ellipse(713,416,200,100);

    //Text before pressing the mouse
    strokeWeight(2);
    fill(255,156,55);
    textSize(50);
    textAlign(RIGHT);
    text('Help me please..', 900, 760);
    textAlign(CENTER);

  }
  strokeWeight(1);
  text("X: "+mouseX, 0, height/4);
  text("Y: "+mouseY, 0, height/2);

  //Now I want to put a face on my emoji

  //The eyes of the emoji
  fill(255,260,255);
  stroke(0);
  strokeWeight(1);
  ellipse(620,230,60,80);
  ellipse(780,230,60,80);
  //The black dot inside of the eyes
  fill(0,0,0);
  stroke(0);
  strokeWeight(1);
  circle(620,230,20);
  circle(780,230,20);

//Eyebrows of the emoji

//Left eyebrow
strokeWeight(5);
stroke(5);
point(661, 148);
point(627, 171);
point(591, 184);
point(563, 184);
strokeWeight(5);

noFill();
strokeWeight(10);
beginShape();
curveVertex(661, 148);
curveVertex(661,148);
curveVertex(627, 171);
curveVertex(591, 184);
curveVertex(563, 184);
curveVertex(563,184);
endShape();

//Right Eyebrows
strokeWeight(5);
point(750, 148);
point(780, 171);
point(810, 184);
point(840, 184);
strokeWeight(5);

noFill();
strokeWeight(10);
beginShape();
curveVertex(750, 148);
curveVertex(750,148);
curveVertex(780, 171);
curveVertex(810, 184);
curveVertex(840, 184);
curveVertex(840,184);
endShape();
}
