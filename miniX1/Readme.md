# This is my first assignment // miniX1
This links to the work I made during miniX1: https://sofiefm.gitlab.io/aesthetic-programming/miniX1/

This links to my repository work: https://gitlab.com/Sofiefm/aesthetic-programming/-/tree/master/miniX1

![Semantic description of image](image.png)

## Description of my work

This is the first mini exercise in the course and the purpose is to get us familiarised with coding in p5js. 
If you go to the first link you will see the work I have produced. I wanted to create something simple, since I have zero experience programming. Somehow I ended up wanting to improve different parts of the coding everytime I finished something — I wanted to create something more or to make it look better. One of my priorities in this program was to make it interactive so the viewer would experience another mobility when using the mouse on their computer. If the mouse touches one of the circles the colour will brighten up immidiatly (this is the interactive part of my program). I quickly realized that this was more difficult than I first expected. 

In the beginning of my programming process I wanted to make just one circle, and then afterwards I wanted to figure out, how to make the first one change colour before making the rest of the circles. Doing it this way I could copy the first part of my code and use for the rest of my circles and just change the numbers in the different variables. I ended up using the Pythagorean theorem to calculate how far the mouse should be from the centre of the circle to make the colour change. After figuring this part out I created variables so I could copy paste it for the rest of the program. 
At this point my focus were to assign the different circles to the right spot on the canvas and to find the right colours for them as well. In the beginning I found it diffucult to figure out where the different coordinates were on the canvas but after the first couple of circles it was a lot easier and everything started to make much more sense! After getting the right placement for the circles it was all about adjusting the colours and try to make the aesthetic look work (at least for me). I just had to change out the numbers in each catgory; red, green and blue, and then find the right combination for my work. I have used the syntax fill and stroke for my cricles and the last number here (red, green, blue, 64) is the alpha, which says something about the transparency of the circles colour. I choose to use the same numer (64) for all the circles, but this is just for the aesthetic outcome of my program. 

My first coding experience has surprisengly been very rewarding. When I think about what I knew one week ago versus what I know today I can see a huge difference. This is motivating for me and I hope that my knowledge within p5.js will grow futher every week. When making this program I decided in advance that I wouldn't copy code from other websites since I know that the best way for me to learn is to write it myself eventhough it may take a while.
My first coding experience has furthermore been a lot of fun. It is a new way for me to write and read and the part I like the most about writing code is that you can change something visually by writing different numbers for example. I still think there is a lot of work for me to do in order to understand how different syntax on a code can impact other codes and how I can optimize my code-writing process.

There must be an easier way to get a lot of different circles in my program? 




