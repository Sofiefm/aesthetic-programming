function setup() {
  // This is my setup code
  createCanvas(640,480);
  print("hello world");
}
function draw() {
  // Here I will put my drawing code
  background("pink");
  //Circle number one
  var x = 75;
  var y = 75;
  var radius = 15;
  var red = 255;
  var green = 0;
  var blue = 0;
  //I use Phytagoras sentence to calculate "d" which is the distance between the center of the circle and mouseX-mouseY)
  var d = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
  if (d <= radius) {
    fill(red, green, blue, 255);
    stroke(red, green, blue, 255);
  } else {
    fill(red, green, blue, 64);
    stroke(red, green, blue, 64);
  }
  circle(x, y, 2*radius);
  // Circle number two
  x = 379;
  y = 200;
  radius = 110;
  red = 0;
  green = 60;
  blue = 255;
  r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
  if (r <= radius) {
    fill(red, green, blue, 255);
    stroke(red, green, blue, 255);
  } else {
    fill(red, green, blue, 64);
    stroke(red, green, blue, 64);
  }
  circle(x, y, 2*radius);
  // Circle number three
  x = 234;
  y = 117;
  radius = 60;
  red = 0;
  green = 234;
  blue = 166;
  r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
  if (r <= radius) {
    fill(red, green, blue, 255);
    stroke(red, green, blue, 255);
  } else {
    fill(red, green, blue, 64);
    stroke(red, green, blue, 64);
  }
  circle(x, y, 2*radius);
  //Circle number four
  x = 150;
  y = 150;
  radius = 60;
  red = 0;
  green = 255;
  blue = 0;
  r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
  if (r <= radius) {
    fill(red, green, blue, 255);
    stroke(red, green, blue, 255);
  } else {
    fill(red, green, blue, 64);
    stroke(red, green, blue, 64);
  }
  circle(x, y, 2*radius);
  //Circle number five
  x = 235;
  y = 200;
  radius = 55;
  red = 200;
  green = 235;
  blue = 255;
  r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
  if (r <= radius) {
    fill(red, green, blue, 255);
    stroke(red, green, blue, 255);
  } else {
    fill(red, green, blue, 64);
    stroke(red, green, blue, 64);
}
    circle(x, y, 2*radius);
    //Circle number six
    x = 20;
    y = 13;
    radius = 20;
    red = 0;
    green = 60;
    blue = 255;
    r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
    if (r <= radius) {
      fill(red, green, blue, 255);
      stroke(red, green, blue, 255);
    } else {
      fill(red, green, blue, 64);
      stroke(red, green, blue, 64);
    }
    circle(x, y, 2*radius);
    //circle number seven
    x = 50;
    y = 150;
    radius = 66;
    red = 0;
    green = 60;
    blue = 255;
    r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
    if (r <= radius) {
      fill(red, green, blue, 255);
      stroke(red, green, blue, 255);
    } else {
      fill(red, green, blue, 64);
      stroke(red, green, blue, 64);
    }
    circle(x, y, 2*radius);
    //circle number eight
    x = 270;
    y = 270;
    radius = 25;
    red = 255;
    green = 0;
    blue = 0;
    r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
    if (r <= radius) {
      fill(red, green, blue, 255);
      stroke(red, green, blue, 255);
    } else {
      fill(red, green, blue, 64);
      stroke(red, green, blue, 64);
    }
    circle(x, y, 2*radius);
    //circle number nine
    x = 480;
    y = 340;
    radius = 110;
    red = 0;
    green = 255;
    blue = 0;
    r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
    if (r <= radius) {
      fill(red, green, blue, 255);
      stroke(red, green, blue, 255);
    } else {
      fill(red, green, blue, 64);
      stroke(red, green, blue, 64);
    }
    circle(x, y, 2*radius);
    //circle number ten
    x = 340;
    y = 300;
    radius = 25;
    red = 0;
    green = 234;
    blue = 166;
    r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
    if (r <= radius) {
      fill(red, green, blue, 255);
      stroke(red, green, blue, 255);
    } else {
      fill(red, green, blue, 64);
      stroke(red, green, blue, 64);
    }
    circle(x, y, 2*radius);
    //circle number 11
    x = 580;
    y = 270;
    radius = 35;
    red = 0;
    green = 234;
    blue = 166;
    r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
    if (r <= radius) {
      fill(red, green, blue, 255);
      stroke(red, green, blue, 255);
    } else {
      fill(red, green, blue, 64);
      stroke(red, green, blue, 64);
    }
    circle(x, y, 2*radius);
    //circle number 12
    x = 575;
    y = 450;
    radius = 30;
    red = 255;
    green = 0;
    blue = 0;
    r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
    if (r <= radius) {
      fill(red, green, blue, 255);
      stroke(red, green, blue, 255);
    } else {
      fill(red, green, blue, 64);
      stroke(red, green, blue, 64);
    }
    circle(x, y, 2*radius);
    //circle number 13
    x = 500;
    y = 250;
    radius = 25;
    red = 200;
    green = 235;
    blue = 255;
    r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
    if (r <= radius) {
      fill(red, green, blue, 255);
      stroke(red, green, blue, 255);
    } else {
      fill(red, green, blue, 64);
      stroke(red, green, blue, 64);
    }
    circle(x, y, 2*radius);
    //circle number 14
    x = 130;
    y = 40;
    radius = 40;
    red = 200;
    green = 235;
    blue = 255;
    r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
    if (r <= radius) {
      fill(red, green, blue, 255);
      stroke(red, green, blue, 255);
    } else {
      fill(red, green, blue, 64);
      stroke(red, green, blue, 64);
    }
    circle(x, y, 2*radius);
    //circle number 15
    x = 600;
    y = 375;
    radius = 25;
    red = 0;
    green = 60;
    blue = 255;
    r = Math.sqrt( (mouseX - x)**2 + (mouseY - y)**2);
    if (r <= radius) {
      fill(red, green, blue, 255);
      stroke(red, green, blue, 255);
    } else {
      fill(red, green, blue, 64);
      stroke(red, green, blue, 64);
    }
    circle(x, y, 2*radius);

}
