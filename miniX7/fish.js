class Fish {
  constructor() {
    this.r = 100; //Size of the fish
    this.x = 500; //Position on the x-axis
    this.y = height - this.r; //position on the y-axis
    this.vy = 0; //velocity of speed along the y-axis
    this.gravity = 2; //adjusting the speed of the fish
  }
  //If jumo function is activated the fish moves up the y-axis
  jump() {
    this.vy = -35;
  }

  hits(plastic) {
    return collideRectRect(this.x, this.y, this.r, this.r, plastic.x, plastic.y, plastic.r, plastic.r);
  }

  move() {
    this.y += this.vy;
    this.vy += this.gravity;
    //The fish is only allowed to move up to a certain point
    this.y = constrain(this.y, 0, height - this.r);
  }

  //To make the fish appear on the canvas 
  show() {
    image(fImg, this.x, this.y, this.r, this.r);
  }
}
