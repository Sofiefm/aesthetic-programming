//My global variables
let fish;
let fImg; //fish image
let pImg; //plastic bag image
let bImg; //background image
let plasticArray = [];
//let score = 0; //something I wanted to use to keep track of the score

function preload() {
  fImg = loadImage('fish.png');
  pImg = loadImage('plastic.png');
  bImg = loadImage('background.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  fish = new Fish();
}

function keyPressed() {
  if (key == ' ') {
    fish.jump(); //calling the jum function from fish.js
  }
}

//also something i tried to use for the score
/*function displayScore(){
  fill(255);
  textSize(20);
  textStyle(NORMAL);
  textAlign(CENTER);
  text("SCORE = " + score, 400, 200); //showing score in right corner
}*/

function draw() {
  if (random(1) < 0.01) { //when to push a new plastic bag
    plasticArray.push(new Plastic());
  }

  background(bImg);
  fish.move();
  fish.show();

  //Instructions for the user
  fill(255, 255, 0);
  textSize(20);
  text("PRESS SPACE TO SAVE ME!", width - 300, 40);
  //Slogan to make the user think about his/her actions.
  fill(0, 230, 255);
  text("THE FUTURE OF THE SEA DEPENDS ON YOU AND ME...", width - 1000, 200);

  for (let p of plasticArray) {
    p.show();
    p.move();
    //displayScore();

    if (fish.hits(p)) {
      noLoop(); //to stop the game
      //Text if the user loose
      textSize(50);
      textAlign(CENTER);
      textStyle(BOLD);
      fill(255);
      text("GAME OVER", width / 2, height / 2);
      textSize(20);
      text("IN THE SEA, THERE IS A DEADLY COMMOTION, ALL CAUSED BY THE TRASH IN THE OCEAN", width / 2, height / 2 + 40);

    }
  }
}
