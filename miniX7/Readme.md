My work: [miniX7](https://sofiefm.gitlab.io/aesthetic-programming/miniX7/)

Repository: [Here](https://gitlab.com/Sofiefm/aesthetic-programming/-/tree/master/miniX7)

## How my game works 
The game I made is simple and therefore possible for almost everyone to try. When playing you have to move the fish up and down the canvas to avoid swimming into incoming obstacles, which is visualized as plastic bags in the ocean. You control the fish by pressing the space button on your computer, and then the fish will “jump” every time you press it. I could have used the arrow keys, but I chose to make it as simple as possible, and I actually also think that only using the space button makes it a bit more difficult. If you hit any of the plastic bags the game is over, and so is the life of fish… :-(

![Semantic description of image](game.png)


#### Inspiration for my game 

I knew from the beginning of this week what I wanted my game to end up like, but I had a hard time figuring out how to start the coding process. I watched a video tutorial by Daniel Shiffman and got very inspired by it: [Inspiration](https://thecodingtrain.com/CodingChallenges/147-chrome-dinosaur.) In this video it is also explained how it is possible to use the collision library for p5js, which is what I have done (look at my index.html). I use this library to make it possible for my program to detect when the fish collides with a plastic bag, this is how the program will know when the game is over. 

## Description of my program

My game consists of two different classes of objects: fish.js and plastic.js
I started out making the class for the fish using a constructor to set up the object by giving it specific instructions. The fish class consist of five different functions/elements: 

1.	Constructor — Is used to give the fish special variables. 
2.	Jump – Makes it possible for the fish to avoid the plastic bags.  
3.	Hits — Detects when a plastic bag hits the fish. 
4.	Move — Is related to jump, but here the gravity is used to make it impossible for the fish to leave the canvas. 
5.	Show — makes the image / png file appear on the canvas. 

I did something very similar with the plastic bag class, but it only had three functions/elements: 
1.	Constructor — Is used to give the plastic bag special variables.
2.	Move — Is used to decide how fast and where is should appear on the canvas. 
3.	Show — makes the image / png file appear on the canvas.

I also made a plasticArray in the sketch.js file which is appearing on the canvas with a random interval. 

## About object-oriented programming (OOP)

Many of the most used programming languages use OOP which is described in The Obscure Objects of Object Orientation by Fuller and Goffey as something programmers always have in the back of their minds. It is a system where we use different objects which are containing data and code, data could be any attributes or properties whereas code is the behavior of the different object. OOP is more than just code, it is about the interplay between the abstract and the concrete or how the objects in code have a relation to the real world, therefore it is also a good opportunity to reflect about the world we live in when working with OOP.

![Semantic description of image](game_over.png)

## My game in a wider perspective 

When I grew up, I played a lot of different games and I really enjoyed it! But when reflecting about the games I played during my childhood I realized that none of them really focused on any serious topics. I found this really surprising, since it would be so easy to create games with a more serious and informative aspect and at the same time make it enjoyable and fun (however, I am not up to date about what options are available today). Even though I know that the game I created is not going to change anything I still think the message is relevant and important. The point is, that I wanted to create an informative and entertaining game, so that young children hopefully will grow up with an incorporated form of awareness about how their behavior affects the earth. I think this game ended up being child friendly which is exactly what I wanted, both the avatar and the quotes / rhymes are easy for children to relate to. To be honest I was kind of skeptical about this way of thinking and making a program, but after I finished, I really think it puts into perspective how different objects relate to each other both in programming and society. Cox & Soon writes:

> "With the abstraction of relations, there is an interplay between abstract and concrete reality. The classifications abstract and concrete were taken up by Marx in his critique of capitalist working conditions to distinguish between abstract labor and living labor. Whereas abstract labor is labor-power exerted producing commodities that uphold capitalism, living labor is the capacity to work. (Cox, Soon, Aesthetic Programming, p. 161). 

This quote exactly describes how OOP is an interplay between abstract and concrete reality. Our real world is so complex and sometimes difficult to understand, that it might seem ridiculous to try to fit all these complex relations into one simple program, but at the other hand it is a way to simplify these complex relations and make them easier to understand. When that is said, I think it is impossible to put all complexities from the real world into a program, something must be lost in the process, and I therefore think it is important to have a critical approach when playing games or reading code. 

I tried to add something to count the score, to make the game more addictive and competitive, but when thinking about my target group it might be all right to omit this part :-P I don’t know why I couldn’t make it work, maybe because I am too tired haha… You can see it in my sketch.js file, if you have any advice or tips let me know <3 <3 

## Reference list 

Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). (on blackboard\Literature)

Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

Slogans in my game: http://www.cetns.ie/News/Marine-Pollution-Slogan-Competition-Results/771/Index.html 

Background: https://clipartart.com/categories/under-ocean-clipart.html 

Inspiration: https://thecodingtrain.com/CodingChallenges/147-chrome-dinosaur 

Fish: https://favpng.com/png_view/fish-cat-kinder-surprise-animated-film-hamster-sakura-haruno-png/gqDHG2nF 

Plastic bag: https://favpng.com/png_view/plastic-bag-plastic-bag-paper-bin-bag-plastic-shopping-bag-png/pLpCNVMg 






