class Plastic {

  constructor() {
    this.r = 100; //size of the plastic bags
    this.x = width - this.r; //placement on the x-axis
    this.y = random(0,height); //placement on the y-axis
  }

  move() {
    this.x -= 16; //to make the plastic bags move backwards
  }

//To make plastic bags appear on the canvas
  show() {
    image(pImg, this.x, this.y, this.r, this.r);
  }
}
