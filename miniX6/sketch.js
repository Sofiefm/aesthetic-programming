//Global variable
var x = 0;

function setup() {
  var numberOfCircles = 60;
  frameRate(15);
  createCanvas(windowWidth, windowHeight);

  circleList = [];
  var minRadius = 15; //Minimum radius of the circle
  var maxRadius = minRadius + 40; //Maximum radius of the circle
  var xMax = windowWidth - maxRadius; //Where the circles should appear on the canvas
  var yMax = windowHeight - maxRadius; //Where the circles should appear on the canvas

  // Here I make a loop to create a list of circles
  for (let i = 0; i < numberOfCircles; i++) {
    circleList.push({
      x: random(xMax), //place circles random on the canvas
      y: random(yMax), //place circles random on the canvas
      radius: random(minRadius) + random(maxRadius),
      color: [random(150, 255), random(150, 255), random(150, 255)], //to use the brighter colors
      startAngle: random(2 * PI) // Angle is used to give each cicle it's own "start angle"
    })

  }
}

// I want to make a list of circle objects created by circle() and change their properties, instead of miniX1 where I kept making new circles manually.

function draw() {
  // Local variable
  var distance;
  x = x + 0.1; // I add 0.1 one to the angle everytime for eachframe
  background("pink");
  // A loop
  for (let i = 0; i < circleList.length; i++) {
    distance = Math.sqrt((mouseX - circleList[i].x) ** 2 + (mouseY - circleList[i].y) ** 2) //how far away the mouse should be from the cente of the circle to "light up" when mouse is drawn over.
    push();
    if (distance <= circleList[i].radius) {
      // I give the circles a high alpha-value, so it is easier to see the interactive part, when the user draw the mouse over them.
      fill(circleList[i].color[0], circleList[i].color[1], circleList[i].color[2], 255);
    } else {
      fill(circleList[i].color[0], circleList[i].color[1], circleList[i].color[2], 100);
    }
    noStroke();
    translate(circleList[i].x, circleList[i].y); // Move to where circle number "i" should be.
    rotate(x + circleList[i].startAngle); //Each circle get a specific angle
    circle(10, 0, 2 * circleList[i].radius); // The circle is drawn 10 pixels to the right of the actual centre of the circle.
    pop();
  }
}
